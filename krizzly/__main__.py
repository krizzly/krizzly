import os
import sys
import json
import socket
import time
import urllib2
import threading

sys.path.append(os.getcwd())
import krizzly

def main():
  user = krizzly.api.account.verify_credentials()
  print "Hello %s, welcome to %s."%(user['name'], 'krizzly')
  latest_tweet_reply = krizzly.api.statuses.home_timeline(count=1)
  tweet=latest_tweet_reply[0]
  krizzly.render(tweet)
  if krizzly.qrender: krizzly.qrender(tweet)

  def _stream_loop():
    backoff = 1
    while True:
        #ss = krizzly.sapi.statuses.sample()
        ss = krizzly.uapi.user()
        friends = ss.next() # dump it
        backoff = 1
        try:
            for s in ss:
                try:
                    krizzly.render(s)
                    if krizzly.qrender: krizzly.qrender(s)
                except UnicodeEncodeError:
                    pass
        except socket.error as e:
            # Usually due to connections being closed
            # i.e. far-end failure
            print e
            time.sleep(1)
        except urllib2.URLError as e:
            # Usually due to missing lookups
            # i.e. near-end failure
            print e
            backoff += 1
            time.sleep(2<<backoff)
  t = threading.Thread(target=_stream_loop)
  t.daemon = True
  t.start()

  if krizzly.qapp: krizzly.qapp.exec_()


if __name__ == '__main__':
  krizzly.init()
  main()
