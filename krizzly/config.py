consumer_key = 'Gc7wJlapvTs6ZQFNo3BMA'
consumer_secret = '4rSPzL17nTph2uuf7wZjskt1kazpwcL7PjfYSQDwI'


##############################################################################
# Do not touch below this line
##############################################################################
import os
import json

class KrizzlyRC(dict):
  def __init__(self, indict=None, filename='~/.krizzlyrc'):
    if indict is None:
      indict = {}
    self._filename = os.path.expanduser(filename)
    dict.__init__(self, indict)
    self.__initialised = True
    self.load(self._filename)

  def load(self, f):
    try:
      F=open(f, 'r')
    except IOError:
      return
    d=json.loads(F.read())
    for k in d.keys():
      self[k]=d[k]

  def save(self, f=None):
    if f is None:
      f = self._filename
    d=json.dumps(self, indent=4)
    with open(f, 'w') as F:
      F.write(d)

  def __getattr__(self, name):
    try: return self.__getitem__(name)
    except KeyError: raise AttributeError(name)
  def __setattr__(self, name, value):
    if not '_KrizzlyRC__initialised' in self.__dict__:
      # Before dict.__init__()
      # Set normal instance attributes in the normal way.
      dict.__setattr__(self, name, value)
    elif name in self.__dict__:
      # After dict.__init__(), lookup instance attributes
      # and set those instead
      dict.__setattr__(self, name, value)
    else:
      # But if the attribute was not defined before dict.__init__()
      # Add it as an item.
      self.__setitem__(name, value)

def reload():
  global _rc
  global access_key
  global access_secret
  _rc = KrizzlyRC()
  access_key =  _rc.access_key
  access_secret =  _rc.access_secret

def save():
  _rc.access_key = access_key
  _rc.access_secret = access_secret

_rc = KrizzlyRC()
_rc.consumer_key = consumer_key
_rc.consumer_secret = consumer_secret
