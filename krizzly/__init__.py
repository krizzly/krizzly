import httplib
import httplib2
import json
import oauth2
import twitter
from twitter.oauth_dance import oauth_dance
import subprocess
from time import sleep
import urllib
from urlparse import parse_qs
from os.path import expanduser
import sys

from config import _rc as config
from functions import *

api = twitter.Twitter(auth=twitter.NoAuth())
sapi= twitter.TwitterStream(auth=twitter.NoAuth())
uapi= None

widget_list = []
qapp = None

def init(interactive=True, graphics=True):
  try:
    auth_ = twitter.OAuth(config.access_key, config.access_secret,
                       config.consumer_key, config.consumer_secret)
  except AttributeError:
    if not interactive:
        raise Exception("I don't have an access token.\n\
Acquiring one needs a human present, you'll do.\n\
You can get one with\n\
    python get_access_token.py")
    print "Do the OAuth 3-legged dance"
    auth_ = None
    # We need to get access tokens.
    access_key, access_secret = oauth_dance("Krizzly",
        config.consumer_key, config.consumer_secret)
    config.access_key = access_key
    config.access_secret = access_secret
    config.save()
    config.load(config._filename)
  else:
    # We try once more
    global api
    global sapi
    global uapi
    # This time, if it fails, we give up by not handling the exception.
    auth = auth_ or twitter.OAuth(config.access_key, config.access_secret,
                         config.consumer_key, config.consumer_secret)
    api = twitter.Twitter(secure=True, auth=auth)
    sapi= twitter.TwitterStream(secure=True, auth=auth)
    uapi= twitter.TwitterStream(domain='userstream.twitter.com',
                                secure=True,
                                auth=auth,
                                api_version='2',
                                block=True)
  if graphics:
   try:
    print "attempting pyside import"
    from PySide.QtGui import QApplication, QPushButton
    from PySide import QtCore
    global qapp
    qapp = QApplication(sys.argv)
    @QtCore.Slot()
    def _show_close_button():
        global widget_list
        pb = QPushButton("Close")
        pb.clicked.connect(qapp.exit)
        pb.show()
        widget_list.append(pb)
    qapp.lastWindowClosed.connect(_show_close_button)
   except ImportError:
    print "but it didn't work, never mind."
