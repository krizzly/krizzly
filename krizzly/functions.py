import twitter
import json
from pprint import pformat
try:
    from PySide import QtCore
    from PySide.QtGui import QLabel
    have_pyside = True
except ImportError:
    have_pyside = False

def render(tob):
  try:
    if tob is None:
        '''non-blocking API can return None objects'''
        return

    if tob.has_key('text'):
        '''This is a normal tweet, it has many fields
        and won't be documented here.'''
        print '%s:(%s)\n\t%s' % (
            tob['user']['screen_name'],
            tob['id_str'],
            tob['text'],
        )
        return

    if tob.has_key('friends'):
        '''This is typically seen at the start of a stream'''
        print "You are following %d friends" % len(tob['friends'])
        return

    if tob.has_key('delete'):
        '''This is typically seen during a stream
        {u'delete': {u'status': {u'id': dddddddddL,
                         u'id_str': u'ddddddddd',
                         u'user_id': xxxxxxxxx,
                         u'user_id_str': u'xxxxxxxxx'}}}'''
        pass

    if tob.has_key('event'):
        if tob['event'] == u'follow':
            print "%s\n\t%s is now following %s" % (
                tob['created_at'],
                tob['source']['screen_name'],
                tob['target']['screen_name'],
            )
            return
        else:
            raise AttributeError(tob['event'])
  except (AttributeError, KeyError) as e:
    #Unidentified structure, pass it on.
    retstr =  "="*70+'\n'
    retstr+= pformat(tob)
    retstr+= '\n'+"="*70
    print retstr
    return retstr

# Keep a reference count so that widgets don't
# fall out of scope, get GC'd and dissappear.
active_widgets = []

if have_pyside:
  @QtCore.Slot(str)
  def _qrender(text):
    l = QLabel(text)
    l.show()
    l.activateWindow()
    active_widgets.append(l)

if have_pyside:
  class Renderer(QtCore.QObject):
    render = QtCore.Signal(str)

  r=Renderer()
  r.render.connect(_qrender)

def qrender(tob):
    if tob is None:
        return

    if tob.has_key('text'):
        r.render.emit('<b>%s</b>: (%s)<p>%s</p>' % (
            tob['user']['screen_name'],
            tob['id_str'],
            tob['text'],
        ))

if not have_pyside:
    qrender = None
