import os
from setuptools import setup

# README.txt from file
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "Krizzly",
    version = "0.4.0",
    author = "Ben Cordero",
    author_email = "bmc@linux.com",
    description = "A simple (read: unfinished) python command line program",
    license = "GPLv3",
    keywords = "krizzly twitter",
    url = "https://gitorious.org/krizzly",
    packages = ['krizzly'],
    long_description = read('README.txt'),
    test_suite='tests',
    classifiers = [
        "Development Status :: 2 - Pre-Alpha",
	"Environment :: Console",
	"Licence :: OSI Approved :: GNU General Public License (GPL)",
    ],
    install_requires = [
	# Actually, we require the version from git.
	# The repository on identifies itself as version 1.7.2, but
	# includes a non-blocking streaming api which is not in
	# pypi (which also identifies as 1.7.2).
	# in a sensible directory, do a
	#   $ git clone https://github.com/sixohsix/twitter.git
	#   $ pip install -e twitter
	# TODO: inform upstream about this.
        'twitter >= 1.7.2',
        'httplib2',
        'oauth2',
    ],
)
