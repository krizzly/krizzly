Krizzly

To use this, you need a twitter access token.
There is no way to automatically fetch one, so you have to do some work.

run
	$ python get_access_token.py

your browser will launch, or you will be asked to go to a url and complete
the oauth transaction on twitter.com.

This only needs to be done once and saves the tokens to ~/.krizzlyrc

If krizzly is installed,
	$ pip install krizzly
then it can be run with
	$ python -m krizzly
or you can run it inplace.
	$ python ./krizzly


